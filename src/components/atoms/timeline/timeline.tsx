import React from "react";

import "./_timeline.scss";

const Timeline = () => {
  return (
    <div className="container_timeline">
      <h2 className="heading-l text-center">
        Votre inscription en{" "}
        <span className="primary-color text-center">quelques secondes.</span>
      </h2>
      <div className="container_steps">
        <div className="box_step">
          <div className="heading_step">
            <div className="step bg-primary basic-light-color heading-l">
              01.
            </div>
            <p className="body-s--bold text-center">Tes informations</p>
          </div>
          <div className="separation"></div>
        </div>
        <div className="box_step">
          <div className="heading_step">
            <div className="step bg-primary basic-light-color heading-l">
              02.
            </div>
            <p className="body-s--bold text-center">Ta formation</p>
          </div>
          <div className="separation"></div>
        </div>
        <div className="box_step">
          <div className="heading_step">
            <div className="step bg-primary basic-light-color heading-l">
              03.
            </div>
            <p className="body-s--bold text-center">Rejoins le Discord ! 🤙</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Timeline;
