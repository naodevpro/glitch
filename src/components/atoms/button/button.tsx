import React from "react";

import "../../../sass/components/_button.scss";

export interface IButton {
  textAction: String;
  onClickMethod: any | null;
  type: String;
}

const Button: React.FunctionComponent<IButton> = ({
  textAction,
  onClickMethod,
  type,
}) => {
  return (
    <div
      className="box_buttons"
      onClick={
        onClickMethod
          ? () => onClickMethod()
          : () => {
              return null;
            }
      }
    >
      <button
        className={`btn ${
          type === "primary" ? "btn--primary" : "btn--primary--light"
        }`}
      >
        {textAction}{" "}
      </button>
      <div
        className={`btn ${
          type === "primary" ? "btn--secondary" : "btn--secondary--light"
        }`}
      >
        {textAction}
      </div>
    </div>
  );
};

export default Button;
