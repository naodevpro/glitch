import React, { useState } from "react";

import "../../../sass/components/_accordion.scss";

export interface IAccordion {
  question: String;
  answer: String;
  type: String;
}

const Accordion: React.FunctionComponent<IAccordion> = ({
  question,
  answer,
  type,
}) => {
  const [active, setActive] = useState(false);
  return (
    <div className={`accordion ${active ? "active" : ""}`}>
      <div className="accordion__title" onClick={() => setActive(!active)}>
        <span
          className={`body-xs ${type === "basic" ? "" : "basic-light-color"}`}
        >
          {question}
        </span>

        <div
          className={`accordion__icon ${
            type === "basic" ? "" : "basic-light-color"
          }`}
        >
          {active ? <i>-</i> : <i>+</i>}
        </div>
      </div>
      <div
        className={`accordion__content body-xs--light ${
          type === "basic" ? "" : "basic-light-color"
        }`}
      >
        {answer}
      </div>
    </div>
  );
};

export default Accordion;
