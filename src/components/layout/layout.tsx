import React from "react";
import FaqInsurance from "../molecules/faqInsurance/faqInsurance";
import Nav from "../organisms/Nav/nav";

type Props = {
  children: React.ReactNode;
};

const Layout = ({ children }: Props) => {
  return (
    <>
      <FaqInsurance />
      <Nav />
      <main>{children}</main>
    </>
  );
};

export default Layout;
