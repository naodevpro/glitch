import React from "react";

import Illustration from "../../../assets/illustrations/illutration_landing.svg";
import Button from "../../atoms/button/button";

import "./_header.scss";

const Header = () => {
  const goToFunnel = () => {
    window.location.href = "https://funnel.glitch-formations.fr/";
  };
  return (
    <div className="container_landing">
      <div className="side_action">
        <h1 className="heading-xxl title_landing">
          Apprendre <span className="primary-color">sans théorie </span>&{" "}
          <span className="primary-color hyphens">coder immédiatement.</span>
        </h1>
        <h5 className="subtitle_landing body-s--light">
          Commencez des projets sans attendre et corrigez ceux de la commuantué
          afin d’avancer.
        </h5>
        <div className="box_field">
          <input
            type="email"
            className="input"
            placeholder="Votre adresse e-mail"
          />
          <Button
            textAction="Clique ici !"
            type={"primary"}
            onClickMethod={goToFunnel}
          />
        </div>
      </div>
      <div className="side_illustration_product">
        <img
          className="illustration_landing"
          src={Illustration}
          alt="illustration"
        />
      </div>
    </div>
  );
};

export default Header;
