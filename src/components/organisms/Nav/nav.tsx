import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

import Logo from "../../../assets/logos/glitch_logo.png";

import "./_nav.scss";

const Nav = () => {
  const [open, setOpen] = useState(true);
  const [heightNav, setHeightNav] = useState("0%");

  const toggleNav = () => {
    setOpen(!open);
    if (open === true) {
      setHeightNav("100%");
    } else {
      setHeightNav("0%");
    }
  };

  const setOpenNav = () => {
    setOpen(true);
    toggleNav();
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const goToFunnel = () => {
    window.location.href = "https://funnel.glitch-formations.fr/";
  };

  return (
    <div className="container_responsive">
      <nav>
        <Link to="/" onClick={() => scrollToTop()}>
          <div className="box_logo">
            <img src={Logo} alt="logo" />
          </div>
        </Link>
        <div
          className={!open ? "nav_burger open" : "nav_burger"}
          onClick={() => toggleNav()}
        >
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div id="myNav" className="overlay" style={{ height: heightNav }}>
          <div className="links">
            <NavLink
              to="/whoWeAre"
              onClick={() => {
                setOpenNav();
              }}
            >
              Qui sommes-nous ?
            </NavLink>
            <NavLink
              to="/howItWorks"
              onClick={() => {
                setOpenNav();
              }}
            >
              Comment ça marche ?
            </NavLink>
            <Link
              to="/ourPedagogy"
              onClick={() => {
                setOpenNav();
              }}
            >
              Notre pédagogie
            </Link>
            <NavLink
              to="/ourFormation"
              onClick={() => {
                setOpenNav();
              }}
            >
              Nos formations
            </NavLink>
            <button className="btn-nav--login btn-nav--mobile bg-basic-light primary-color primary-border">
              <span className="body-s--bold">Connexion</span>
            </button>
            <button
              className="btn-nav--call-to-action btn-nav--mobile bg-primary basic-light-color"
              onClick={() => {
                goToFunnel();
              }}
            >
              <span className="body-s--bold">Inscription</span>
            </button>
          </div>
        </div>
        <div className="nav_links_desktop">
          <NavLink to="/whoWeAre">
            <h2>Qui sommes-nous ? </h2>
          </NavLink>
          <NavLink to="/howItWorks">
            <h2>Comment ça marche</h2>
          </NavLink>
          <Link to="/ourPedagogy">
            <h2>Notre pédagogie</h2>
          </Link>
          <NavLink to="/ourFormations">
            <h2> Nos formations</h2>
          </NavLink>
          <button className="btn-nav--login bg-basic-light primary-color primary-border">
            <span className="body-s--bold">Connexion</span>
          </button>
          <button className="btn-nav--call-to-action bg-primary basic-light-color">
            <span
              className="body-s--bold"
              onClick={() => {
                goToFunnel();
              }}
            >
              Inscription
            </span>
          </button>
        </div>
      </nav>
    </div>
  );
};

export default Nav;
