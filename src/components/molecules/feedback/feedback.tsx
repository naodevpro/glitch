import React from "react";

import "./_feedback.scss";

import FeedbackIcon from "../../../assets/icons/feedback_icon.svg";

function Feedback() {
  return (
    <div className="container_feedback bg-secondary-light">
      <h2 className="heading-l">Vos avis comptent.</h2>
      <div className="box_feedback_cards">
        <div className="card--small bg-basic-light feedback_card ">
          <img src={FeedbackIcon} alt="feedback_icon" />
          <p className="body-s--bold text-center">
            Un grand merci à la communauté. La méthode de partage de
            connaissance et le coaching personnel par e-mail boost réellement la
            motivation 😗 ! <span className="primary-color">Anaïs</span>
          </p>
        </div>
        <div className="card--small bg-basic-light feedback_card ">
          <img src={FeedbackIcon} alt="feedback_icon" />
          <p className="body-s--bold text-center">
            Un grand merci à la communauté. La méthode de partage de
            connaissance et le coaching personnel par e-mail boost réellement la
            motivation 🚀 ! <span className="primary-color">Anaïs</span>
          </p>
        </div>
        <div className="card--small bg-basic-light feedback_card ">
          <img src={FeedbackIcon} alt="feedback_icon" />
          <h6 className="body-s--bold text-center">
            Un grand merci à la communauté. La méthode de partage de
            connaissance et le coaching personnel par e-mail boost réellement la
            motivation 🧑🏻‍🚀 ! <span className="primary-color">Anaïs</span>
          </h6>
        </div>
      </div>
    </div>
  );
}

export default Feedback;
