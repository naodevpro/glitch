import React from "react";
import { useNavigate } from "react-router-dom";

import "./_faqInsurance.scss";

function FaqInsurance() {
  let navigate = useNavigate();
  const showFAQ = () => {
    navigate(`/faq/`);
  };
  return (
    <div className="container_faq_insurance bg-secondary-light">
      <h6 className="body-s text-center">
        Découvrez le fonctionnement de la plateforme à travers notre FAQ
        <span
          className="body-s--bold underline"
          onClick={() => {
            showFAQ();
          }}
        >
          🤙 Je fonce{" "}
        </span>
      </h6>
    </div>
  );
}

export default FaqInsurance;
