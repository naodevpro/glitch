import React, { useState } from "react";
import Accordion from "../../atoms/accordion/accordion";
import Button from "../../atoms/button/button";

import "./_moreInfosPedagogy.scss";

function MoreInfosPedagogy() {
  const [faqs] = useState([
    {
      question: "Est-ce que mes informations son sécurisées ? ",
      answer:
        "Tenetur ullam rerum ad iusto possimus sequi mollitia dolore sunt quam praesentium. Tenetur ullam rerum ad iusto possimus sequi mollitia dolore sunt quam praesentium.Tenetur ullam rerum ad iusto possimus sequi mollitia dolore sunt quam praesentium.",
    },
    {
      question: "Comment faire si j’ai besoin d’aide lors d’un projet ?",
      answer:
        "Aperiam ab atque incidunt dolores ullam est, earum ipsa recusandae velit cumque. Aperiam ab atque incidunt dolores ullam est, earum ipsa recusandae velit cumque.",
    },
    {
      question:
        "Je veux une suite à la formation carrière afin de devenir un expert sur des sujets plus complexe, comment faire ?",
      answer:
        "Blanditiis aliquid adipisci quisquam reiciendis voluptates itaque.",
    },
    {
      question:
        "Les formations que vous proposez ne sont pas certifiante alors que m’offre t’elles ?",
      answer:
        "Blanditiis aliquid adipisci quisquam reiciendis voluptates itaque.",
    },
  ]);
  return (
    <div className="container_more_infos_pedagogy bg-basic-dark-color">
      <h2 className="heading-l basic-light-color text-center">
        En savoir plus sur notre pédagogie.
      </h2>
      <div className="box_accordion">
        {faqs.map((faq, index) => {
          return (
            <Accordion
              key={index}
              question={faq.question}
              answer={faq.answer}
              type={"dark"}
            />
          );
        })}
      </div>
      <Button
        textAction={"Continuer"}
        onClickMethod={null}
        type={"secondary"}
      />
    </div>
  );
}

export default MoreInfosPedagogy;
