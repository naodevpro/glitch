import React from "react";

import rocketIcon from "../../../assets/iconography/rocket.svg";
import enjoyIcon from "../../../assets/iconography/discord.svg";
import peerIcon from "../../../assets/iconography/validation.svg";

import "./_wordingAction.scss";

function WordingAction() {
  return (
    <div className="container_responsive">
      <div className="container_wording_action">
        <h2 className="heading-l text-center">
          <span className="primary-color">Apprennez</span>,{" "}
          <span className="primary-color">construisez</span> &{" "}
          <span className="primary-color">gagnez</span> autrement.
        </h2>
        <div className="container_wording">
          <div className="box_wording">
            <img src={rocketIcon} alt="wording-icon" />
            <h3 className="body-s--bold text-center">
              Une montée en compétences{" "}
              <span className="primary-color">garantie.</span>
            </h3>
          </div>
          <div className="box_wording">
            <img src={enjoyIcon} alt="wording-icon" />
            <h3 className="body-s--bold text-center">
              Une communauté
              <span className="primary-color"> active</span> &
              <span className="primary-color"> engagée.</span>
            </h3>
          </div>
          <div className="box_wording">
            <img src={peerIcon} alt="wording-icon" />
            <h3 className="body-s--bold text-center">
              Une pédagogie en
              <span className="primary-color"> peer to peer.</span>
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
}

export default WordingAction;
