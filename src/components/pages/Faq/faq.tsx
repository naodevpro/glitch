import React, { useState } from "react";

import Accordion from "../../atoms/accordion/accordion";
import Icon from "../../../assets/icons/validate.svg";
import "./_faq.scss";

function Faq() {
  const [faqs] = useState([
    {
      question: "Est-ce que mes informations son sécurisées ? ",
      answer:
        "Tenetur ullam rerum ad iusto possimus sequi mollitia dolore sunt quam praesentium. Tenetur ullam rerum ad iusto possimus sequi mollitia dolore sunt quam praesentium.Tenetur ullam rerum ad iusto possimus sequi mollitia dolore sunt quam praesentium.",
    },
    {
      question: "Comment faire si j’ai besoin d’aide lors d’un projet ?",
      answer:
        "Aperiam ab atque incidunt dolores ullam est, earum ipsa recusandae velit cumque. Aperiam ab atque incidunt dolores ullam est, earum ipsa recusandae velit cumque.",
    },
    {
      question:
        "Je veux une suite à la formation carrière afin de devenir un expert sur des sujets plus complexe, comment faire ?",
      answer:
        "Blanditiis aliquid adipisci quisquam reiciendis voluptates itaque.",
    },
    {
      question:
        "Les formations que vous proposez ne sont pas certifiante alors que m’offre t’elles ?",
      answer:
        "Blanditiis aliquid adipisci quisquam reiciendis voluptates itaque.",
    },
  ]);

  return (
    <div className="container_responsive">
      <div className="container_faq">
        <h1 className="heading-l text-center">
          Tes données t’appartiennent.{" "}
          <span className="primary-color">Nous ne les vendons pas.</span>
        </h1>
        <div className="box_cards">
          <div className="card--small" style={{ backgroundColor: "#E8DAFE" }}>
            <div className="card_heading">
              <img src={Icon} alt="icon" />
              <h6 className="body-s--bold">Tes données.</h6>
            </div>
            <p className="body-xs">
              Tes données sont cryptées, inaccessibles et stockées sur des
              serveurs européens.
            </p>
          </div>
          <div className="card--small" style={{ backgroundColor: "#C5A4FC" }}>
            <div className="card_heading">
              <img src={Icon} alt="icon" />
              <h6 className="body-s--bold">Nos Partenaires.</h6>
            </div>
            <p className="body-xs">
              Nos partenaires informatiques sont parmi les plus reconnus du
              marché et leurs sécurité est auditée régulièrement.
            </p>
          </div>
          <div
            className="card--small"
            style={{ backgroundColor: "#6224FA", color: "#fff" }}
          >
            <div className="card_heading">
              <img src={Icon} alt="icon" />
              <h6 className="body-s--bold">Tes identifiants.</h6>
            </div>
            <p className="body-xs">
              Notre architecture dispose de plusieurs degrés de chiffrement qui
              assurent, que personne ne peut accéder à tes identifiants.
            </p>
          </div>
        </div>
        <h1 className="heading-l text-center">
          Questions les <span className="primary-color">plus fréquentes.</span>
        </h1>
        <div className="box_accordion">
          {faqs.map((faq, index) => {
            return (
              <Accordion
                key={index}
                question={faq.question}
                answer={faq.answer}
                type={"basic"}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Faq;
