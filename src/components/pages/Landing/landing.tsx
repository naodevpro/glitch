import React from "react";

import IconDiscord from "../../../assets/icons/discord.svg";
import Timeline from "../../atoms/timeline/timeline";
import Feedback from "../../molecules/feedback/feedback";
import MoreInfosPedagogy from "../../molecules/moreInfosPedagogy/moreInfosPedaogy";
import WordingAction from "../../molecules/wordingAction/wordingAction";
import Header from "../../organisms/Header/header";

import "./_landing.scss";

function Landing() {
  return (
    <div className="container_responsive">
      <Header />
      <div className="container_joinCommunity bg-secondary-light">
        <img src={IconDiscord} alt="icon discord" />
        <p className="body-s--light underline text-center">
          Rejoignez <span className="body-s--bold">la communauté</span> sur
          Discord <span className="body-s--bold">gratuitement 🤙</span>{" "}
        </p>
      </div>
      <WordingAction />
      <MoreInfosPedagogy />
      <Timeline />
      <Feedback />
    </div>
  );
}

export default Landing;
