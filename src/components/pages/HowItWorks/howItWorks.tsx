import React from "react";

import signupIllustration from "../../../assets/illustrations/signup.svg";
import briefIllustration from "../../../assets/iconography/mail.svg";
import reviewIllustration from "../../../assets/iconography/programming.svg";
import learnToLearnIllustration from "../../../assets/iconography/bulb.svg";
import targetIllustration from "../../../assets/iconography/target.svg";

import stepOneBackground from "../../../assets/illustrations/01.png";
import stepTwoBackground from "../../../assets/illustrations/02.png";
import stepThreeBackground from "../../../assets/illustrations/03.png";
import stepFourBackground from "../../../assets/illustrations/04.png";
import stepFiveBackground from "../../../assets/illustrations/05.png";

import "./_howItWorks.scss";

function HowItWorks() {
  return (
    <div className="container_responsive">
      <div className="container_how_it_works">
        <div className="box_heading">
          <h1 className="heading-l">
            Comment <span className="primary-color">ça marche.</span>
          </h1>
          <div className="separation_bar"></div>
        </div>
        <div className="container_steps">
          <div className="box_step box_step--left">
            <div
              className="box_step_description bg-step--left"
              style={{ backgroundImage: "url(" + stepOneBackground + ")" }}
            >
              <h3 className="heading-m step-text--left">
                Inscris-toi en{" "}
                <span className="primary-color">60 secondes.</span>
              </h3>
              <p className="body-s step-text--left">
                Tes données sont cryptées, inaccessibles et stockées sur des
                serveurs européens.
              </p>
            </div>
            <div className="box_step_asset">
              <img src={signupIllustration} alt="asset_step" />
            </div>
          </div>
          <div className="box_step box_step box_step--right">
            <div className="box_step_asset">
              <img src={briefIllustration} alt="asset_step" />
            </div>
            <div
              className="box_step_description bg-step--right"
              style={{ backgroundImage: "url(" + stepTwoBackground + ")" }}
            >
              <h3 className="heading-m step-text--right">
                Commence à recevoir{" "}
                <span className="primary-color">tes briefs.</span>
              </h3>
              <p className="body-s step-text--right">
                Chaque semaine, des nouveaux briefs de projets arriveront dans
                ta boite mail. À toi de relever les défis et de corriger ceux de
                tes camarades
              </p>
            </div>
          </div>
          <div className="box_step box_step box_step--left">
            <div
              className="box_step_description bg-step--left"
              style={{ backgroundImage: "url(" + stepThreeBackground + ")" }}
            >
              <h3 className="heading-m step-text--left">
                <span className="primary-color">Review les briefs </span>
                d’apprenants.
              </h3>
              <p className="body-s step-text--left">
                Afin d’avancer dans ton aventure et de comprendre tous les
                enjeux, on te propose de corriger les mêmes projets que tu
                réalise mais qui appartiennes à d’autres apprenants pour te
                familiariser avec le code et les review.
              </p>
            </div>
            <div className="box_step_asset">
              <img src={reviewIllustration} alt="asset_step" />
            </div>
          </div>
          <div className="box_step box_step box_step--right">
            <div className="box_step_asset">
              <img src={learnToLearnIllustration} alt="asset_step" />
            </div>
            <div
              className="box_step_description bg-step--right"
              style={{ backgroundImage: "url(" + stepFourBackground + ")" }}
            >
              <h3 className="heading-m step-text--right">
                Apprends à <span className="primary-color">apprendre.</span>
              </h3>
              <p className="body-s step-text--right">
                Au fur et à mesure des briefs, nous t’apprenons à te débrouiller
                comme dans le monde professionnel.
              </p>
            </div>
          </div>
          <div className="box_step">
            <div
              className="box_step_description bg-step--left"
              style={{ backgroundImage: "url(" + stepFiveBackground + ")" }}
            >
              <h3 className="heading-m step-text--left">
                Développe{" "}
                <span className="primary-color">ton projet personnel.</span>
              </h3>
              <p className="body-s step-text--left">
                Afin d’avancer dans ton aventure et de comprendre tous les
                enjeux, on te propose de corriger les mêmes projets que tu
                réalise mais qui appartiennes à d’autres apprenants pour te
                familiariser avec le code et les review.
              </p>
            </div>
            <div className="box_step_asset">
              <img
                className="rocket_illustration"
                src={targetIllustration}
                alt="asset_step"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HowItWorks;
