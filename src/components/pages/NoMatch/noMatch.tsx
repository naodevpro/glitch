import React from "react";
import { useNavigate } from "react-router-dom";

import Button from "../../atoms/button/button";

import "./_noMatch.scss";

function NoMatch() {
  let navigate = useNavigate();

  const showFAQ = () => {
    navigate(`/faq/`);
  };

  return (
    <div className="container_noMatch">
      <div className="box_message">
        <h1 className="title_no_match heading-xxl--extra-bold primary-color">
          Oups<span>.</span>
        </h1>
      </div>
      <div className="box_call_to_action">
        <Button
          textAction={"Lire la FAQ !"}
          onClickMethod={showFAQ}
          type={"primary"}
        />
      </div>
    </div>
  );
}

export default NoMatch;
