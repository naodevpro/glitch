import React from "react";
import { Routes, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import Layout from "./components/layout/layout";

import Faq from "./components/pages/Faq/faq";
import HowItWorks from "./components/pages/HowItWorks/howItWorks";
import Landing from "./components/pages/Landing/landing";
import NoMatch from "./components/pages/NoMatch/noMatch";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Layout>
          <Routes>
            <Route path="/" element={<Landing />} />
            <Route path="/faq" element={<Faq />} />
            <Route path="/howItWorks" element={<HowItWorks />} />
            <Route path="*" element={<NoMatch />} />
          </Routes>
        </Layout>
      </BrowserRouter>
    </div>
  );
}

export default App;
